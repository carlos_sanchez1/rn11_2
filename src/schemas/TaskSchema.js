import {ObjectId} from 'bson';

class Task {
  /**
   *
   * @param {string} name The name of the task
   * @param {string status The status of the task. Default value is "Open"
   * @param {ObjectId} id The ObjectId to create this task with
   */
  constructor({
    name,
    partition,
    status = Task.STATUS_OPEN,
    id = new ObjectId(),
  }) {
    this._partition = partition;
    this._id = id;
    this.name = name;
    this.status = status;
  }

  static STATUS_OPEN = 'Open';
  static STATUS_IN_PROGRESS = 'InProgress';
  static STATUS_COMPLETE = 'Complete';
  static schema = {
    name: 'Task',
    properties: {
      _id: 'objectId',
      _partition: 'string',
      name: 'string',
      status: 'string',
    },
    primaryKey: '_id',
  };
}

export {Task};

// const TaskSchema = {
//   name: 'Task',
//   primaryKey: 'id',
//   properties: {
//     id: 'string',
//     alarmNotifIds: {type: 'list', objectType: 'int'},
//     name: 'string',
//     color: 'string',
//     mode: 'int',
//     done: 'bool',
//     icon: 'string',
//     pomodoro: 'string',
//     filter: 'Filter',
//     soundYear: 'int',
//     soundMonth: 'int',
//     soundDay: 'int',
//     soundHour: 'int',
//     soundMinute: 'int',
//     subtasks: {type: 'list', objectType: 'Subtask'},
//   },
// };

// const SubtaskSchema = {
//   name: 'Subtask',
//   primaryKey: 'id',
//   properties: {
//     id: 'string',
//     name: 'string',
//     done: 'bool',
//   },
// };

// const FilterSchema = {
//   name: 'Filter',
//   primaryKey: 'id',
//   properties: {
//     id: 'string',
//     name: 'string',
//   },
// };

// export {TaskSchema, SubtaskSchema, FilterSchema};
