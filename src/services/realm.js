import React, {useState} from 'react';
import Realm from 'realm';
import {getRealmApp} from './getRealmApp';
const app = getRealmApp();

import {Task, SubtaskSchema, FilterSchema} from '../schemas/TaskSchema';
import {RoutineSchema} from '../schemas/RoutineSchema';
import {
  CourseSchema,
  FlashCardSchema,
  NotificationStudySchema,
  RepetitionTimeSchema,
  PomodoroSchema,
  ExamsSchema,
  NotificationsExamsSchema,
} from '../schemas/CourseSchema';

export default function getRealm() {
  return Realm.open({
    schema: [Task.schema],
    schemaVersion: 1,
    sync: {
      user: user,
      // partitionValue: "myPartition",
    },
  });
}
